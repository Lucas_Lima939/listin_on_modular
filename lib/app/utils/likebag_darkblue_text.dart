
import 'package:flutter/material.dart';
import 'likebag_theme_app.dart';

class LikeBagDarkBlueText extends StatelessWidget {
  final String labelText;
  final FontWeight fontWeight;
  final double fontSize;

  const LikeBagDarkBlueText(this.labelText, {this.fontWeight, this.fontSize});

  @override
  Widget build(BuildContext context) {
    return Text(
      labelText,
      maxLines: 3,
      overflow: TextOverflow.ellipsis,
      style: TextStyle(
          color: LikeBagThemeApp.darkBlue,
          fontWeight: fontWeight,
          fontSize: fontSize
          ),
    );
  }
}

