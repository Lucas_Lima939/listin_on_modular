class LikeBagMessages {
  static String ERROR_COSTUMER_NAME_OBRIGATORY =
      "Informe corretamente o nome do cliente.";
  static String ERROR_CATEGORY_NAME_OBRIGATORY =
      "O nome da categoria deve ser informado.";
  static String ERROR_ATTRIBUTE_NAME_OBRIGATORY =
      "Nome do atributo deve ser informado.";
  static String ERROR_PAYMENT_LINK_DESCRIPTION_OBRIGATORY =
      "O produto e a categoria devem ser informados.";
  static String ERROR_ATTRIBUTE_NAME_TERMS_OBRIGATORY =
      "Nome do atributo e valores devem ser informados.";
  static String ERROR_NOT_DECTECTED = "Ocorreu um erro não identificado.";
  static String ERROR_MAIN_IMAGE_OBRIGATORY =
      "A imagem principal deve ser selecionada.";
  static String ERROR_PRODUCT_REF_OBRIGATORY =
      "REF do produto deve ser informada.";
  static String ERROR_PRODUCT_NAME_OBRIGATORY =
      "O nome do produto deve ser informado.";
  static String ERROR_PRODUCT_STOCK_OBRIGATORY =
      "A quantidade em estoque do produto deve ser informado.";
  static String ERROR_PRODUCT_PRICE_OBRIGATORY =
      "O preço do produto deve ser informado.";
  static String ERROR_ATTRIBUTE_DELETE =
      "Ocorreu um erro ao excluir o atributo!";
  static String ERROR_ATTRIBUTE_TERM_DELETE =
      "Ocorreu um erro ao excluir o termo do atributo!";
  static String ERROR_CATEGORY_DELETE =
      "Ocorreu um erro ao excluir a categoria, por favor, tente novamente!";
  static String ERROR_ATTRIBUTE_CREATE =
      "Ocorreu um erro ao criar nova categoria, por favor, tente novamente!";
  static String ERROR_CATEGORY_CREATE =
      "Ocorreu um erro ao criar nova categoria, por favor, tente novamente!";
  static String ERROR_CATEGORY_DUPLICATE_NAME =
      "Erro ao incluir categoria, o nome de categoria já existe!";
  static String ERROR_CATEGORY_DELETE_DEFAULT =
      "Erro: A categoria padrão de produto não pode ser excluída.";
  static String ERROR_LOGIN = "Insira um e-mail válido";
  static String ERROR_LOGIN_PW =
      "Senha inválida, deve conter pelo menos 4 caracteres";
  static String ERROR_PRODUCT_INSERT = "Não foi possível incluír produto.";
  static String ERROR_INTERNET_FAIL_SEARCH =
      "Falha de conexão com a internet, dados não podem ser recuperados.";
  static String ERROR_INTERNET_FAIL =
      "Falha de conexão com a internet, ação não pode ser realizada.";
  static String ERROR_DELETE_PRODUCT =
      "Erro ao excluir produto, por favor tente novamente.";

  static String ERROR_PRODUCT_SHIPPING = "O frete máximo é R\$900!";
  static String ERROR_INTEREST_RATE = "O juros máximo é 100%!";
  static String ERROR_PRICE_WRONG =
      "O preço deve estar entre R\$1,00 e R\$9000!";
  static String ERROR_DESCRIPTION_WRONG =
      "O produto e a quantidade devem ser descritos!";

  static String HINT_CATEGORY_NAME = "Informe o nome da categoria.";
  static String HINT_COSTUMER_NAME = "Informe o nome do cliente.";
  static String HINT_PAYMENT_LINK_DESCRIPTION = "Camiseta - 1un.";
  static String HINT_CATEGORY_DESCRIPTION = "Informe a descrição da categoria.";
  static String HINT_ATTRIBUTE_TERM = "Informe o valor do atributo.";
  static String HINT_ATTRIBUTE_NAME = "Informe o nome do atributo.";
  static String HINT_PRODUCT_DESCRIPTION =
      "Informe a descrição detalhada do produto.";
  static String HINT_WEIGHT = "Informe o peso (em Kg).";
  static String HINT_HEIGHT = "Informe a altura (em cm).";
  static String HINT_WIDTH = "Informe a largura (em cm).";
  static String HINT_LENGHT = "Informe o comprimento (em cm).";
  static String HINT_SEARCH = "Pesquisar";

  static String SUCESS_ADD_CATEGORY = "Categoria %s incluída com sucesso!";
  static String SUCESS_ADD_ATTRIBUTE = "Atributo incluído com sucesso!";
  static String SUCESS_DELETE_PRODUCT = "Produto excluído com sucesso!";
  static String SUCESS_DELETE_CATEGORY = "Categoria excluída com sucesso!";
  static String SUCESS_DELETE_ATTRIBUTE = "Atributo excluído com sucesso!";
  static String SUCESS_DELETE_ATTRIBUTE_TERM =
      "Termo do atributo excluído com sucesso!";
  static String SUCESS_EDIT_PRODUCT = "Produto editado com sucesso!";
  static String SUCESS_ADD_PRODUCT = "Produto %s adicionado com sucesso!";

  static String QUESTION_DELETE = "Tem certeza que deseja excluir?";
  static String QUESTION_DELETE_CATEGORY =
      "Tem certeza que deseja excluir a categoria %s?";
  static String QUESTION_DELETE_ATTRIBUTE =
      "Tem certeza que deseja excluir o atributo %s?";

  static String INFO_NOT_REVERT_OPERATION =
      "A operação não poderá ser revertida.";
  static String INFO_DELETE_PRODUCT = "Excluindo, aguarde um instante...";
  static String INFO_MAX_IMAGES = "Número máximo de imagens atingido.";
  static String INFO_WITHOUT_DATA = "Nenhum dado encontrado.";
  static String INFO_ADD_PRODUCT = "Adicionando produto, aguarde.";
  static String INFO_ADD_NEW_PRODUCT = "Adicionar novo produto.";
  static String INFO_CANT_DELETE_ATTRIBUTE_TERM =
      "%s está sendo utilizado para %s produto(s) e por isto não poderá ser excluído.";
  static String INFO_CANT_DELETE_ATTRIBUTE =
      "O atributo está sendo utilizado em um ou mais produtos e por isto não poderá ser excluído.";
  static String INFO_EDIT_PRODUCT = "Editando produto, aguarde um instante...";
  static String INFO_ADD_CATEGORY =
      "Adicionando categoria, aguarde um instante...";
  static String INFO_DELETE_CATEGORY =
      "Excluindo categoria, aguarde um instante...";
  static String INFO_ADD_ATTRIBUTE =
      "Adicionando atributo, aguarde um instante...";
  static String INFO_DELETE_ATTRIBUTE =
      "Excluindo atributo, aguarde um instante...";
}
