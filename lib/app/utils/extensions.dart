import 'package:listing_with_slidy/app/modules/products_list/products_list_controller.dart';

extension ExtractName on OrderBy {
  String extractName() {
    return this.toString().split('.')[1].toLowerCase();
  }

  String toName() {
    switch (this) {
      case OrderBy.ID:
        return 'Id';
        break;
      case OrderBy.TITLE:
        return 'Nome';
        break;
      default:
        return 'Data';
        break;
    }
  }
}
