import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'likebag_theme_app.dart';

class LikeBagUtil {
  static String STRING_EMPTY = "";
  static SnackBar snackbar;

  static String cleanText(String texto) {
    texto = texto.replaceAll("<p>", "");
    texto = texto.replaceAll("</p>", "");
    texto = texto.replaceAll("<br>", "");
    texto = texto.replaceAll("<br />", "");

    return texto;
  }

  static String trataStringPreco(String p) {
    if ((p.endsWith(".0")) ||
        (p.endsWith(".1")) ||
        (p.endsWith(".2")) ||
        (p.endsWith(".3")) ||
        (p.endsWith(".4")) ||
        (p.endsWith(".5")) ||
        (p.endsWith(".6")) ||
        (p.endsWith(".7")) ||
        (p.endsWith(".8")) ||
        (p.endsWith(".9"))) {
      p = p + "0";
    } else if (p.contains(".") == false) {
      p = p + ".00";
    }
    return p;
  }

  static Color getListTileColor(List list, String text) {
    if (list.contains(text)) {
      return LikeBagThemeApp.blue;
    } else {
      return Colors.transparent;
    }
  }

  static Color getListTextColor(List list, String text) {
    if (list.contains(text)) {
      return Colors.white;
    } else {
      return LikeBagThemeApp.blue;
    }
  }

  static String getSubString(String name, int endIndex, {int beginIndex}) {
    int lenght = name.length;

    if (lenght < endIndex) {
      return name;
    } else {
      return name.substring(beginIndex != null ? beginIndex : 0, endIndex);
    }
  }

  static String toFirstLetterUpperCase(String text) {
    return text[0].toUpperCase() + text.substring(1);
  }

  static DateTime dateWooCommerceToString(date) {
    return DateFormat("yyyy-MM-ddThh:mm").parse(date);
  }

  static String convertCurrency(double value) {
    NumberFormat brlCurrency =
        NumberFormat.currency(locale: 'pt_BR', name: 'R\$');
    return brlCurrency.format(value);
  }

  static showMessage(String message, BuildContext context) {
    snackbar = SnackBar(content: Text(message), duration: new Duration(seconds: 2));
    Scaffold.of(context).showSnackBar(snackbar);
  }
}
