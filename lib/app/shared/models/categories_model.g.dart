// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'categories_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Categories _$CategoriesFromJson(Map<String, dynamic> json) {
  return Categories(
    id: json['id'] as int,
    name: json['name'] as String,
    slug: json['slug'] as String,
    parent: json['parent'] as int,
    description: json['description'] as String,
    display: json['display'] as String,
    image: json['image'] == null
        ? null
        : Images.fromJson(json['image'] as Map<String, dynamic>),
    menuOrder: json['menuOrder'] as int,
    count: json['count'] as int,
    lLinks: json['lLinks'] == null
        ? null
        : Links.fromJson(json['lLinks'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$CategoriesToJson(Categories instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('id', instance.id);
  writeNotNull('name', instance.name);
  writeNotNull('slug', instance.slug);
  writeNotNull('parent', instance.parent);
  writeNotNull('description', instance.description);
  writeNotNull('display', instance.display);
  writeNotNull('image', instance.image);
  writeNotNull('menuOrder', instance.menuOrder);
  writeNotNull('count', instance.count);
  writeNotNull('lLinks', instance.lLinks);
  return val;
}
