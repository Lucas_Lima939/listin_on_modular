import 'package:json_annotation/json_annotation.dart';
import 'package:listing_with_slidy/app/shared/models/images_model.dart';
import 'package:listing_with_slidy/app/shared/models/product_variation_attribute_model.dart';

part 'product_variation_model.g.dart';

@JsonSerializable(nullable: true, includeIfNull: false)
class ProductVariation {
  String regular_price;
  Images image;
  List<ProductVariationAttribute> attributes;
  bool manageStock;
  int stockQuantity;

  ProductVariation({this.regular_price, this.image, this.attributes, this.manageStock = true, this.stockQuantity});

  factory ProductVariation.fromJson(Map<String, dynamic> json) =>
      _$ProductVariationFromJson(json);

  Map<String, dynamic> toJson() => _$ProductVariationToJson(this);
}
