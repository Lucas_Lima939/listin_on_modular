// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Product _$ProductFromJson(Map<String, dynamic> json) {
  return Product(
    id: json['id'] as int,
    name: json['name'] as String,
    slug: json['slug'] as String,
    permalink: json['permalink'] as String,
    dateCreated: json['dateCreated'] as String,
    dateCreatedGmt: json['dateCreatedGmt'] as String,
    dateModified: json['dateModified'] as String,
    dateModifiedGmt: json['dateModifiedGmt'] as String,
    type: json['type'] as String,
    status: json['status'] as String,
    featured: json['featured'] as bool,
    catalogVisibility: json['catalogVisibility'] as String,
    description: json['description'] as String,
    shortDescription: json['shortDescription'] as String,
    sku: json['sku'] as String,
    price: json['price'] as String,
    regular_price: json['regular_price'] as String,
    salePrice: json['salePrice'] as String,
    dateOnSaleFrom: json['dateOnSaleFrom'] as String,
    dateOnSaleFromGmt: json['dateOnSaleFromGmt'] as String,
    dateOnSaleTo: json['dateOnSaleTo'] as String,
    dateOnSaleToGmt: json['dateOnSaleToGmt'] as String,
    priceHtml: json['priceHtml'] as String,
    onSale: json['onSale'] as bool,
    purchasable: json['purchasable'] as bool,
    totalSales: json['totalSales'] as int,
    virtual: json['virtual'] as bool,
    downloadable: json['downloadable'] as bool,
    downloads: json['downloads'] as List,
    downloadLimit: json['downloadLimit'] as int,
    downloadExpiry: json['downloadExpiry'] as int,
    externalUrl: json['externalUrl'] as String,
    buttonText: json['buttonText'] as String,
    taxStatus: json['taxStatus'] as String,
    taxClass: json['taxClass'] as String,
    manageStock: json['manageStock'] as bool,
    stockQuantity: json['stockQuantity'] as int,
    stockStatus: json['stockStatus'] as String,
    backorders: json['backorders'] as String,
    backordersAllowed: json['backordersAllowed'] as bool,
    backordered: json['backordered'] as bool,
    soldIndividually: json['soldIndividually'] as bool,
    weight: json['weight'] as String,
    dimensions: json['dimensions'] == null
        ? null
        : Dimensions.fromJson(json['dimensions'] as Map<String, dynamic>),
    shippingRequired: json['shippingRequired'] as bool,
    shippingTaxable: json['shippingTaxable'] as bool,
    shippingClass: json['shippingClass'] as String,
    shippingClassId: json['shippingClassId'] as int,
    reviewsAllowed: json['reviewsAllowed'] as bool,
    averageRating: json['averageRating'] as String,
    ratingCount: json['ratingCount'] as int,
    relatedIds: json['relatedIds'] as List,
    upsellIds: json['upsellIds'] as List,
    crossSellIds: json['crossSellIds'] as List,
    parentId: json['parentId'] as int,
    purchaseNote: json['purchaseNote'] as String,
    categories: (json['categories'] as List)
        ?.map((e) =>
            e == null ? null : Categories.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    tags: (json['tags'] as List)
        ?.map(
            (e) => e == null ? null : Tags.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    images: (json['images'] as List)
        ?.map((e) =>
            e == null ? null : Images.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    attributes: (json['attributes'] as List)
        ?.map((e) => e == null
            ? null
            : AttributeProduct.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    defaultAttributes: json['defaultAttributes'] as List,
    variations: json['variations'] as List,
    groupedProducts: json['groupedProducts'] as List,
    menuOrder: json['menuOrder'] as int,
    metaData: json['metaData'] as List,
    lLinks: json['lLinks'] == null
        ? null
        : Links.fromJson(json['lLinks'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ProductToJson(Product instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('id', instance.id);
  writeNotNull('name', instance.name);
  writeNotNull('slug', instance.slug);
  writeNotNull('permalink', instance.permalink);
  writeNotNull('dateCreated', instance.dateCreated);
  writeNotNull('dateCreatedGmt', instance.dateCreatedGmt);
  writeNotNull('dateModified', instance.dateModified);
  writeNotNull('dateModifiedGmt', instance.dateModifiedGmt);
  writeNotNull('type', instance.type);
  writeNotNull('status', instance.status);
  writeNotNull('featured', instance.featured);
  writeNotNull('catalogVisibility', instance.catalogVisibility);
  writeNotNull('description', instance.description);
  writeNotNull('shortDescription', instance.shortDescription);
  writeNotNull('sku', instance.sku);
  writeNotNull('price', instance.price);
  writeNotNull('regular_price', instance.regular_price);
  writeNotNull('salePrice', instance.salePrice);
  writeNotNull('dateOnSaleFrom', instance.dateOnSaleFrom);
  writeNotNull('dateOnSaleFromGmt', instance.dateOnSaleFromGmt);
  writeNotNull('dateOnSaleTo', instance.dateOnSaleTo);
  writeNotNull('dateOnSaleToGmt', instance.dateOnSaleToGmt);
  writeNotNull('priceHtml', instance.priceHtml);
  writeNotNull('onSale', instance.onSale);
  writeNotNull('purchasable', instance.purchasable);
  writeNotNull('totalSales', instance.totalSales);
  writeNotNull('virtual', instance.virtual);
  writeNotNull('downloadable', instance.downloadable);
  writeNotNull('downloads', instance.downloads);
  writeNotNull('downloadLimit', instance.downloadLimit);
  writeNotNull('downloadExpiry', instance.downloadExpiry);
  writeNotNull('externalUrl', instance.externalUrl);
  writeNotNull('buttonText', instance.buttonText);
  writeNotNull('taxStatus', instance.taxStatus);
  writeNotNull('taxClass', instance.taxClass);
  writeNotNull('manageStock', instance.manageStock);
  writeNotNull('stockQuantity', instance.stockQuantity);
  writeNotNull('stockStatus', instance.stockStatus);
  writeNotNull('backorders', instance.backorders);
  writeNotNull('backordersAllowed', instance.backordersAllowed);
  writeNotNull('backordered', instance.backordered);
  writeNotNull('soldIndividually', instance.soldIndividually);
  writeNotNull('weight', instance.weight);
  writeNotNull('dimensions', instance.dimensions);
  writeNotNull('shippingRequired', instance.shippingRequired);
  writeNotNull('shippingTaxable', instance.shippingTaxable);
  writeNotNull('shippingClass', instance.shippingClass);
  writeNotNull('shippingClassId', instance.shippingClassId);
  writeNotNull('reviewsAllowed', instance.reviewsAllowed);
  writeNotNull('averageRating', instance.averageRating);
  writeNotNull('ratingCount', instance.ratingCount);
  writeNotNull('relatedIds', instance.relatedIds);
  writeNotNull('upsellIds', instance.upsellIds);
  writeNotNull('crossSellIds', instance.crossSellIds);
  writeNotNull('parentId', instance.parentId);
  writeNotNull('purchaseNote', instance.purchaseNote);
  writeNotNull('categories', instance.categories);
  writeNotNull('tags', instance.tags);
  writeNotNull('images', instance.images);
  writeNotNull('attributes', instance.attributes);
  writeNotNull('defaultAttributes', instance.defaultAttributes);
  writeNotNull('variations', instance.variations);
  writeNotNull('groupedProducts', instance.groupedProducts);
  writeNotNull('menuOrder', instance.menuOrder);
  writeNotNull('metaData', instance.metaData);
  writeNotNull('lLinks', instance.lLinks);
  return val;
}
