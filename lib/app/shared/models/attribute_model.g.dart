// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'attribute_model.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AttributeModel on _AttributeTermsBase, Store {
  final _$attributeTermsAtom = Atom(name: '_AttributeTermsBase.attributeTerms');

  @override
  ObservableList<AttributeTerms> get attributeTerms {
    _$attributeTermsAtom.reportRead();
    return super.attributeTerms;
  }

  @override
  set attributeTerms(ObservableList<AttributeTerms> value) {
    _$attributeTermsAtom.reportWrite(value, super.attributeTerms, () {
      super.attributeTerms = value;
    });
  }

  final _$_AttributeTermsBaseActionController =
      ActionController(name: '_AttributeTermsBase');

  @override
  dynamic setAttributeTerms(ObservableList<AttributeTerms> value) {
    final _$actionInfo = _$_AttributeTermsBaseActionController.startAction(
        name: '_AttributeTermsBase.setAttributeTerms');
    try {
      return super.setAttributeTerms(value);
    } finally {
      _$_AttributeTermsBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  Map<String, dynamic> toJson() {
    final _$actionInfo = _$_AttributeTermsBaseActionController.startAction(
        name: '_AttributeTermsBase.toJson');
    try {
      return super.toJson();
    } finally {
      _$_AttributeTermsBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
attributeTerms: ${attributeTerms}
    ''';
  }
}
