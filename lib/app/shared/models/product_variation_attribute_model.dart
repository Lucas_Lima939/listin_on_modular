import 'package:json_annotation/json_annotation.dart';
part 'product_variation_attribute_model.g.dart';

@JsonSerializable(nullable: true, includeIfNull: false)
class ProductVariationAttribute {
    int id;
    int position;
    bool visible;
    bool variation;
    String name;
    String option;

    ProductVariationAttribute({this.id, this.name, this.option, this.position, this.variation = true, this.visible = true});

  factory ProductVariationAttribute.fromJson(Map<String, dynamic> json) =>
      _$ProductVariationAttributeFromJson(json);

  Map<String, dynamic> toJson() => _$ProductVariationAttributeToJson(this);
}
