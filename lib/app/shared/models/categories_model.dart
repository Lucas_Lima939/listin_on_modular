import 'package:json_annotation/json_annotation.dart';
import 'package:listing_with_slidy/app/shared/models/images_model.dart';
import 'package:listing_with_slidy/app/shared/models/product_links_model.dart';

part 'categories_model.g.dart';

@JsonSerializable(nullable: true, includeIfNull: false)
class Categories {
  int id;
  String name;
  String slug;
  int parent;
  String description;
  String display;
  Images image;
  int menuOrder;
  int count;
  Links lLinks;

  Categories(
      {this.id,
      this.name,
      this.slug,
      this.parent,
      this.description,
      this.display,
      this.image,
      this.menuOrder,
      this.count,
      this.lLinks});

  factory Categories.fromJson(Map<String, dynamic> json) =>
      _$CategoriesFromJson(json);

  Map<String, dynamic> toJson() => _$CategoriesToJson(this);
}
