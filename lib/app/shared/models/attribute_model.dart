import 'attribute_terms_model.dart';
import 'product_links_model.dart';

import 'package:mobx/mobx.dart';
part 'attribute_model.g.dart';

class AttributeModel = _AttributeTermsBase with _$AttributeModel;

abstract class _AttributeTermsBase with Store {
  int id;
  String name;
  String slug;
  String type;
  String orderBy;
  bool hasArchives;
  Links lLinks;

  @observable
  ObservableList<AttributeTerms> attributeTerms = ObservableList<AttributeTerms>();

  @action
  setAttributeTerms(ObservableList<AttributeTerms> value)=> attributeTerms.addAll(value);

  _AttributeTermsBase(
      {this.id,
      this.name,
      this.slug,
      this.type,
      this.orderBy,
      this.hasArchives,
      this.attributeTerms,
      this.lLinks});

  @action
  _AttributeTermsBase.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    slug = json['slug'];
    type = json['type'];
    orderBy = json['order_by'];
    hasArchives = json['has_archives'];
    if (json['attributeTerms'] != null) {
      attributeTerms = new List<AttributeTerms>();
      json['attributeTerms'].forEach((v) {
        attributeTerms.add(new AttributeTerms.fromJson(v));
      });
    }
    lLinks = json['_links'] != null ? new Links.fromJson(json['_links']) : null;
  }


  @action
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['slug'] = this.slug;
    data['type'] = this.type;
    data['order_by'] = this.orderBy;
    data['has_archives'] = this.hasArchives;
    if (this.attributeTerms != null) {
      data['attributeTerms'] =
          this.attributeTerms.map((v) => v.toJson()).toList();
    }
    if (this.lLinks != null) {
      data['_links'] = this.lLinks.toJson();
    }
    return data;
  }
  
}

