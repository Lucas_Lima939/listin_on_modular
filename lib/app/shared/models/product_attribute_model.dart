import 'package:json_annotation/json_annotation.dart';


part 'product_attribute_model.g.dart';

@JsonSerializable(nullable: true,includeIfNull: false)
class AttributeProduct {
  int id;
  String name;
  int position;
  bool visible;
  bool variation;
  List<String> options;

  AttributeProduct({this.id, this.name, this.position, this.visible,
      this.variation, this.options});

  factory AttributeProduct.fromJson(Map<String, dynamic> json) => _$AttributeProductFromJson(json);

  Map<String, dynamic> toJson() => _$AttributeProductToJson(this);

}