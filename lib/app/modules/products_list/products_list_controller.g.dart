// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'products_list_controller.dart';

// **************************************************************************
// InjectionGenerator
// **************************************************************************

final $ProductsListController = BindInject(
  (i) => ProductsListController(),
  singleton: true,
  lazy: true,
);

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ProductsListController on _ProductsListControllerBase, Store {
  Computed<bool> _$isFilteringComputed;

  @override
  bool get isFiltering =>
      (_$isFilteringComputed ??= Computed<bool>(() => super.isFiltering,
              name: '_ProductsListControllerBase.isFiltering'))
          .value;
  Computed<bool> _$searchIsEmptyComputed;

  @override
  bool get searchIsEmpty =>
      (_$searchIsEmptyComputed ??= Computed<bool>(() => super.searchIsEmpty,
              name: '_ProductsListControllerBase.searchIsEmpty'))
          .value;

  final _$selectedFilterAtom =
      Atom(name: '_ProductsListControllerBase.selectedFilter');

  @override
  AttributeTerms get selectedFilter {
    _$selectedFilterAtom.reportRead();
    return super.selectedFilter;
  }

  @override
  set selectedFilter(AttributeTerms value) {
    _$selectedFilterAtom.reportWrite(value, super.selectedFilter, () {
      super.selectedFilter = value;
    });
  }

  final _$searchResultProductsAtom =
      Atom(name: '_ProductsListControllerBase.searchResultProducts');

  @override
  ObservableList<Product> get searchResultProducts {
    _$searchResultProductsAtom.reportRead();
    return super.searchResultProducts;
  }

  @override
  set searchResultProducts(ObservableList<Product> value) {
    _$searchResultProductsAtom.reportWrite(value, super.searchResultProducts,
        () {
      super.searchResultProducts = value;
    });
  }

  final _$productsAtom = Atom(name: '_ProductsListControllerBase.products');

  @override
  ObservableList<Product> get products {
    _$productsAtom.reportRead();
    return super.products;
  }

  @override
  set products(ObservableList<Product> value) {
    _$productsAtom.reportWrite(value, super.products, () {
      super.products = value;
    });
  }

  final _$attributesAtom = Atom(name: '_ProductsListControllerBase.attributes');

  @override
  ObservableList<AttributeModel> get attributes {
    _$attributesAtom.reportRead();
    return super.attributes;
  }

  @override
  set attributes(ObservableList<AttributeModel> value) {
    _$attributesAtom.reportWrite(value, super.attributes, () {
      super.attributes = value;
    });
  }

  final _$isLoadingAtom = Atom(name: '_ProductsListControllerBase.isLoading');

  @override
  bool get isLoading {
    _$isLoadingAtom.reportRead();
    return super.isLoading;
  }

  @override
  set isLoading(bool value) {
    _$isLoadingAtom.reportWrite(value, super.isLoading, () {
      super.isLoading = value;
    });
  }

  final _$indexAtom = Atom(name: '_ProductsListControllerBase.index');

  @override
  int get index {
    _$indexAtom.reportRead();
    return super.index;
  }

  @override
  set index(int value) {
    _$indexAtom.reportWrite(value, super.index, () {
      super.index = value;
    });
  }

  final _$selectedOrderByAtom =
      Atom(name: '_ProductsListControllerBase.selectedOrderBy');

  @override
  OrderBy get selectedOrderBy {
    _$selectedOrderByAtom.reportRead();
    return super.selectedOrderBy;
  }

  @override
  set selectedOrderBy(OrderBy value) {
    _$selectedOrderByAtom.reportWrite(value, super.selectedOrderBy, () {
      super.selectedOrderBy = value;
    });
  }

  final _$orderAtom = Atom(name: '_ProductsListControllerBase.order');

  @override
  String get order {
    _$orderAtom.reportRead();
    return super.order;
  }

  @override
  set order(String value) {
    _$orderAtom.reportWrite(value, super.order, () {
      super.order = value;
    });
  }

  final _$orderingAtom = Atom(name: '_ProductsListControllerBase.ordering');

  @override
  bool get ordering {
    _$orderingAtom.reportRead();
    return super.ordering;
  }

  @override
  set ordering(bool value) {
    _$orderingAtom.reportWrite(value, super.ordering, () {
      super.ordering = value;
    });
  }

  final _$hasMoreAtom = Atom(name: '_ProductsListControllerBase.hasMore');

  @override
  bool get hasMore {
    _$hasMoreAtom.reportRead();
    return super.hasMore;
  }

  @override
  set hasMore(bool value) {
    _$hasMoreAtom.reportWrite(value, super.hasMore, () {
      super.hasMore = value;
    });
  }

  final _$refreshAsyncAction =
      AsyncAction('_ProductsListControllerBase.refresh');

  @override
  Future<void> refresh({bool ordering = false}) {
    return _$refreshAsyncAction.run(() => super.refresh(ordering: ordering));
  }

  final _$loadProductsAsyncAction =
      AsyncAction('_ProductsListControllerBase.loadProducts');

  @override
  Future<void> loadProducts({String orderByParam, String defineListOrder}) {
    return _$loadProductsAsyncAction.run(() => super.loadProducts(
        orderByParam: orderByParam, defineListOrder: defineListOrder));
  }

  final _$loadAttributesAsyncAction =
      AsyncAction('_ProductsListControllerBase.loadAttributes');

  @override
  Future<void> loadAttributes() {
    return _$loadAttributesAsyncAction.run(() => super.loadAttributes());
  }

  final _$loadProductsFilteredAsyncAction =
      AsyncAction('_ProductsListControllerBase.loadProductsFiltered');

  @override
  Future<void> loadProductsFiltered(
      AttributeModel attribute, AttributeTerms attributeTerms) {
    return _$loadProductsFilteredAsyncAction
        .run(() => super.loadProductsFiltered(attribute, attributeTerms));
  }

  final _$loadAttributesTermsAsyncAction =
      AsyncAction('_ProductsListControllerBase.loadAttributesTerms');

  @override
  Future<void> loadAttributesTerms() {
    return _$loadAttributesTermsAsyncAction
        .run(() => super.loadAttributesTerms());
  }

  final _$productsSearchAsyncAction =
      AsyncAction('_ProductsListControllerBase.productsSearch');

  @override
  Future<void> productsSearch(String search) {
    return _$productsSearchAsyncAction.run(() => super.productsSearch(search));
  }

  final _$_ProductsListControllerBaseActionController =
      ActionController(name: '_ProductsListControllerBase');

  @override
  void setIndex(int value) {
    final _$actionInfo = _$_ProductsListControllerBaseActionController
        .startAction(name: '_ProductsListControllerBase.setIndex');
    try {
      return super.setIndex(value);
    } finally {
      _$_ProductsListControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setOrder(String value) {
    final _$actionInfo = _$_ProductsListControllerBaseActionController
        .startAction(name: '_ProductsListControllerBase.setOrder');
    try {
      return super.setOrder(value);
    } finally {
      _$_ProductsListControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setSelectedFilter(AttributeTerms value) {
    final _$actionInfo = _$_ProductsListControllerBaseActionController
        .startAction(name: '_ProductsListControllerBase.setSelectedFilter');
    try {
      return super.setSelectedFilter(value);
    } finally {
      _$_ProductsListControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void clearFilter() {
    final _$actionInfo = _$_ProductsListControllerBaseActionController
        .startAction(name: '_ProductsListControllerBase.clearFilter');
    try {
      return super.clearFilter();
    } finally {
      _$_ProductsListControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void clearProducts() {
    final _$actionInfo = _$_ProductsListControllerBaseActionController
        .startAction(name: '_ProductsListControllerBase.clearProducts');
    try {
      return super.clearProducts();
    } finally {
      _$_ProductsListControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setProducts(ObservableList<Product> produto) {
    final _$actionInfo = _$_ProductsListControllerBaseActionController
        .startAction(name: '_ProductsListControllerBase.setProducts');
    try {
      return super.setProducts(produto);
    } finally {
      _$_ProductsListControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setAttributes(ObservableList<AttributeModel> attribute) {
    final _$actionInfo = _$_ProductsListControllerBaseActionController
        .startAction(name: '_ProductsListControllerBase.setAttributes');
    try {
      return super.setAttributes(attribute);
    } finally {
      _$_ProductsListControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic changeLoading(bool value) {
    final _$actionInfo = _$_ProductsListControllerBaseActionController
        .startAction(name: '_ProductsListControllerBase.changeLoading');
    try {
      return super.changeLoading(value);
    } finally {
      _$_ProductsListControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setOrderBy(OrderBy value) {
    final _$actionInfo = _$_ProductsListControllerBaseActionController
        .startAction(name: '_ProductsListControllerBase.setOrderBy');
    try {
      return super.setOrderBy(value);
    } finally {
      _$_ProductsListControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic changeHasMore(bool value) {
    final _$actionInfo = _$_ProductsListControllerBaseActionController
        .startAction(name: '_ProductsListControllerBase.changeHasMore');
    try {
      return super.changeHasMore(value);
    } finally {
      _$_ProductsListControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic changeOrdering(bool value) {
    final _$actionInfo = _$_ProductsListControllerBaseActionController
        .startAction(name: '_ProductsListControllerBase.changeOrdering');
    try {
      return super.changeOrdering(value);
    } finally {
      _$_ProductsListControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic clearSearchResults() {
    final _$actionInfo = _$_ProductsListControllerBaseActionController
        .startAction(name: '_ProductsListControllerBase.clearSearchResults');
    try {
      return super.clearSearchResults();
    } finally {
      _$_ProductsListControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
selectedFilter: ${selectedFilter},
searchResultProducts: ${searchResultProducts},
products: ${products},
attributes: ${attributes},
isLoading: ${isLoading},
index: ${index},
selectedOrderBy: ${selectedOrderBy},
order: ${order},
ordering: ${ordering},
hasMore: ${hasMore},
isFiltering: ${isFiltering},
searchIsEmpty: ${searchIsEmpty}
    ''';
  }
}
