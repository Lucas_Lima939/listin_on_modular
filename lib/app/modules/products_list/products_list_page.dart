import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:listing_with_slidy/app/modules/products_list/products_list_module.dart';
import 'package:listing_with_slidy/app/modules/products_list/widgets/order_by_modal.dart';
import 'package:listing_with_slidy/app/modules/products_list/widgets/product_card.dart';
import 'package:listing_with_slidy/app/modules/products_list/widgets/products_list_modal_bottom_sheet.dart';
import 'package:listing_with_slidy/app/utils/likebag_theme_app.dart';
import 'package:mobx/mobx.dart';

import 'products_list_controller.dart';
import 'search_page.dart';
import 'widgets/drawer.dart';
import 'widgets/products_cus_shimmer.dart';

class ProductsListPage extends StatefulWidget {
  @override
  _ProductsListPageState createState() => _ProductsListPageState();
}

class _ProductsListPageState
    extends ModularState<ProductsListPage, ProductsListController> {
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    autorun((_) {
      if (controller.products.length - controller.index == 5 &&
          controller.hasMore) {
        controller.loadProducts();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(backgroundColor: LikeBagThemeApp.darkBlue, actions: [
        IconButton(
            icon: Icon(Icons.search),
            onPressed: () =>
                showSearch(context: context, delegate: DataSearch()))
      ]),
      body: Column(children: [
        Row(
          children: [
            FlatButton(
                onPressed: () => showModalBottomSheet(
                    builder: (_) => ProductsListModalBottomSheet(),
                    context: context),
                child: Row(children: [
                  Icon(Icons.filter_list),
                  SizedBox(width: 5),
                  Text('Filtrar'),
                ])),
            FlatButton(
                onPressed: () => showModalBottomSheet(
                    builder: (_) => OrderByModal(), context: context),
                child: Row(children: [
                  Icon(Icons.format_list_numbered),
                  SizedBox(width: 5),
                  Text('Ordenar')
                ])),
          ],
        ),
        Expanded(child: Container(child: Observer(builder: (_) {
          if (controller.isLoading)
            return ListView.builder(
                itemCount: 10, itemBuilder: (_, __) => ProductsCusShimmer());
          else
            return RefreshIndicator(
              backgroundColor: Colors.white,
              onRefresh: () => controller.refresh(),
              child: ListView.builder(
                itemExtent: 125,
                itemCount: controller.products.length + 1,
                itemBuilder: (_, index) {
                  controller.setIndex(index);
                  if (index < controller.products.length) {
                    return ProductCard(
                      product: controller.products[index],
                    );
                  } else if (controller.hasMore) {
                    return Padding(
                        padding: EdgeInsets.symmetric(vertical: 32.0),
                        child: Center(child: CircularProgressIndicator()));
                  } else {
                    return Padding(
                        padding: EdgeInsets.symmetric(vertical: 32.0),
                        child: Center(child: Text('nada mais para carregar')));
                  }
                },
              ),
            );
        })))
      ]),
    ));
  }
}
