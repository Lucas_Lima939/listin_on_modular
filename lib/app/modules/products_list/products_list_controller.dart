import 'package:flutter/material.dart';
import 'package:listing_with_slidy/app/shared/models/attribute_model.dart';
import 'package:listing_with_slidy/app/shared/models/attribute_terms_model.dart';
import 'package:listing_with_slidy/app/shared/models/product_model.dart';
import 'package:mobx/mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:listing_with_slidy/app/utils/extensions.dart';

import 'products_list_repository.dart';

part 'products_list_controller.g.dart';

enum ProductsTabState { IDLE, FILTER }

enum OrderBy { DATE, ID, TITLE }

@Injectable()
class ProductsListController = _ProductsListControllerBase
    with _$ProductsListController;

abstract class _ProductsListControllerBase with Store {
  ProductsListRepository repo = ProductsListRepository();

  int _page = 1;

  _ProductsListControllerBase() {
    loadProducts();
    loadAttributes();
  }

  @observable
  AttributeTerms selectedFilter;

  @observable
  ObservableList<Product> searchResultProducts = ObservableList<Product>();

  @observable
  ObservableList<Product> products = ObservableList<Product>();

  @observable
  ObservableList<AttributeModel> attributes = ObservableList<AttributeModel>();

  @observable
  var isLoading = false;

  @observable
  int index = 0;

  @observable
  var selectedOrderBy = OrderBy.DATE;

  @observable
  String order = 'desc';

  @observable
  bool ordering = false;

  @observable
  bool hasMore = true;

  @action
  void setIndex(int value) => index = value;

  @action
  void setOrder(String value) => order = value;

  @action
  void setSelectedFilter(AttributeTerms value) => selectedFilter = value;

  @action
  void clearFilter() {
    selectedFilter = null;
    Modular.to.pushNamed('/');
  }

  @action
  void clearProducts() => products = ObservableList<Product>();

  @action
  void setProducts(ObservableList<Product> produto) => products.addAll(produto);

  @action
  void setAttributes(ObservableList<AttributeModel> attribute) =>
      attributes.addAll(attribute);

  @action
  changeLoading(bool value) => isLoading = value;

  @action
  setOrderBy(OrderBy value) => selectedOrderBy = value;

  @action
  changeHasMore(bool value) => hasMore = value;

  @action
  changeOrdering(bool value) => ordering = value;

  @action
  Future<void> refresh({bool ordering = false}) async {
    products.clear();
    _page = 1;
    changeOrdering(ordering);
    await loadProducts();
  }

  @action
  Future<void> loadProducts(
      {String orderByParam, String defineListOrder}) async {
    if (_page == 1) {
      changeLoading(true);
    }
    if (!ordering) {
      selectedOrderBy = OrderBy.DATE;
      order = 'desc';
    }
    final orderBy = selectedOrderBy.extractName();
    var response = await repo.getProductsPerPage(20, _page,
        orderBy: orderBy, order: order);

    if (response.success) {
      changeHasMore(response.data.length >= 20);
      setProducts(response.data);
      if (_page == 1) {
        changeLoading(false);
      }
      _page++;
    } else {
      if (_page == 1) {
        changeLoading(false);
      }
    }
  }

  @action
  Future<void> loadAttributes() async {
    var response = await repo.getAllAtributes();
    if (response.success) {
      setAttributes(response.data);
      loadAttributesTerms();
    } else {
      return;
    }
  }

  @action
  Future<void> loadProductsFiltered(
      AttributeModel attribute, AttributeTerms attributeTerms) async {
    changeLoading(true);
    setSelectedFilter(attributeTerms);
    var response =
        await repo.getProductsFiltered(attribute.slug, attributeTerms.id);
    if (response.success) {
      //clearProducts();
      setProducts(response.data);
      changeLoading(false);
    } else {
      changeLoading(false);
    }
  }

  @action
  Future<void> loadAttributesTerms() async {
    attributes.forEach((attribute) async => await repo
        .getAllAtributesTerms(attribute.id)
        .then((attributeTerms) => attribute.setAttributeTerms(attributeTerms)));
  }

  @action
  clearSearchResults() => searchResultProducts = ObservableList<Product>();

  @action
  Future<void> productsSearch(String search) async {
    if (search.trim().isEmpty) {
      return;
    } else {
      changeLoading(true);
      searchResultProducts.addAll(_filter(search.trim()));
      changeLoading(false);
      await _searchOnServer(search.trim())
          .then((results) => searchResultProducts.addAll(results));
    }
  }

  Future<ObservableList<Product>> _searchOnServer(String search) async {
    return await repo.getProductsSearch(search).then((jsonList) {
      ObservableList<Product> responseList = jsonList
          .map((json) => Product.fromJson(json))
          .toList()
          .asObservable();
      responseList.removeWhere((productRetrieved) => products
          .map((productLoaded) => productLoaded.id)
          .toList()
          .contains(productRetrieved.id));
      return responseList;
    });
  }

  ObservableList<Product> _filter(String search) {
    ObservableList<Product> filteredProducts =
        List<Product>.from(products).asObservable();
    filteredProducts.retainWhere((product) {
      return product.name.toUpperCase().contains(search.toUpperCase());
    });
    return filteredProducts;
  }

  @computed
  bool get isFiltering => selectedFilter != null;

  @computed
  bool get searchIsEmpty => searchResultProducts.isEmpty;
}
