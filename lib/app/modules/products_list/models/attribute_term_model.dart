import 'package:listing_with_slidy/app/shared/models/attribute_terms_model.dart';
import 'package:mobx/mobx.dart';

import 'package:mobx/mobx.dart';
part 'attribute_term_model.g.dart';

class AttributeTermModel = _AttributeTermModelBase with _$AttributeTermModel;

abstract class _AttributeTermModelBase with Store {
  @observable
  int attributeId;
  @action
  setAttributeId(int value) => attributeId = value;

  @observable
  AttributeTerms attributeTerm;
  @action
  setAttributeTerm(AttributeTerms value) => attributeTerm = value;
}
