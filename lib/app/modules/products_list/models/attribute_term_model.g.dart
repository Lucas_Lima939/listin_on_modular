// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'attribute_term_model.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AttributeTermModel on _AttributeTermModelBase, Store {
  final _$attributeIdAtom = Atom(name: '_AttributeTermModelBase.attributeId');

  @override
  int get attributeId {
    _$attributeIdAtom.reportRead();
    return super.attributeId;
  }

  @override
  set attributeId(int value) {
    _$attributeIdAtom.reportWrite(value, super.attributeId, () {
      super.attributeId = value;
    });
  }

  final _$attributeTermAtom =
      Atom(name: '_AttributeTermModelBase.attributeTerm');

  @override
  AttributeTerms get attributeTerm {
    _$attributeTermAtom.reportRead();
    return super.attributeTerm;
  }

  @override
  set attributeTerm(AttributeTerms value) {
    _$attributeTermAtom.reportWrite(value, super.attributeTerm, () {
      super.attributeTerm = value;
    });
  }

  final _$_AttributeTermModelBaseActionController =
      ActionController(name: '_AttributeTermModelBase');

  @override
  dynamic setAttributeId(int value) {
    final _$actionInfo = _$_AttributeTermModelBaseActionController.startAction(
        name: '_AttributeTermModelBase.setAttributeId');
    try {
      return super.setAttributeId(value);
    } finally {
      _$_AttributeTermModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setAttributeTerm(AttributeTerms value) {
    final _$actionInfo = _$_AttributeTermModelBaseActionController.startAction(
        name: '_AttributeTermModelBase.setAttributeTerm');
    try {
      return super.setAttributeTerm(value);
    } finally {
      _$_AttributeTermModelBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
attributeId: ${attributeId},
attributeTerm: ${attributeTerm}
    ''';
  }
}
