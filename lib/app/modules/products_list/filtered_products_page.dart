import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:listing_with_slidy/app/modules/products_list/widgets/order_by_modal.dart';
import 'package:listing_with_slidy/app/utils/likebag_theme_app.dart';

import 'products_list_controller.dart';
import 'search_page.dart';
import 'widgets/product_card.dart';
import 'widgets/products_cus_shimmer.dart';
import 'widgets/products_list_modal_bottom_sheet.dart';

class FilteredProductsPage extends StatefulWidget {
  static String routeName = '/filtered_products_list';
  @override
  _FilteredProductsPageState createState() => _FilteredProductsPageState();
}

class _FilteredProductsPageState
    extends State<FilteredProductsPage> {
      ProductsListController controller = Modular.get();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: LikeBagThemeApp.darkBlue,
          actions: [
          IconButton(
              icon: Icon(Icons.search),
              onPressed: () =>
                  showSearch(context: context, delegate: DataSearch()))
        ]),
        body: Container(
          child: Column(children: [
            Row(children: [
              FlatButton(
                  onPressed: () => showModalBottomSheet(
                      builder: (_) => ProductsListModalBottomSheet(),
                      context: context),
                  child: Row(children: [
                    Icon(Icons.filter_list),
                    SizedBox(width: 5),
                    Text('Filtrar'),
                  ])),
              FlatButton(
                  onPressed: () => showModalBottomSheet(
                      builder: (_) => OrderByModal(), context: context),
                  child: Row(children: [
                    Icon(Icons.format_list_numbered),
                    SizedBox(width: 5),
                    Text('Ordenar')
                  ])),
              Visibility(
                visible: controller.isFiltering,
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                  decoration: BoxDecoration(
                      color: LikeBagThemeApp.darkBlue,
                      borderRadius: BorderRadius.all(Radius.circular(13.0))),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          controller.selectedFilter.name,
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(width: 10),
                        GestureDetector(
                          onTap: () => controller.clearFilter(),
                          child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(20)),
                              child: Icon(Icons.close,
                                  color: LikeBagThemeApp.darkBlue, size: 15)),
                        )
                      ]),
                ),
              )
            ]),
            Expanded(
              child: Observer(
                builder: (_) {
                  if (!controller.isLoading)
                    return ListView(
                      children: controller.products
                          .map((product) => ProductCard(
                                product: product,
                              ))
                          .toList(),
                    );
                  else if (controller.products.isEmpty)
                    return Center(child: Text('Nenhum produto encontrado'));
                  else
                    return ListView.builder(
                        itemCount: 10,
                        itemBuilder: (_, __) => ProductsCusShimmer());
                },
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
