import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:listing_with_slidy/app/shared/models/product_model.dart';
import 'package:listing_with_slidy/app/utils/likebag_theme_app.dart';
import 'package:mobx/mobx.dart';
import 'products_list_controller.dart';
import 'widgets/product_card.dart';
import 'widgets/products_cus_shimmer.dart';

class DataSearch extends SearchDelegate<String> {
  ProductsListController controller = ProductsListController();

  @override
  String get searchFieldLabel => 'Pesquisa';

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
          icon: Icon(Icons.clear),
          onPressed: () {
            controller.clearSearchResults();
            query = '';
            showSuggestions(context);
          })
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
        icon: Icon(Icons.arrow_back_ios),
        onPressed: () => Navigator.pop(context));
  }

  @override
  Widget buildResults(BuildContext context) {
    controller.productsSearch(query);
    return Container(
      color: Colors.white,
      child: Observer(
        builder: (_) {
          if (controller.isLoading)
            return ListView.builder(
                itemCount: 10, itemBuilder: (_, __) => ProductsCusShimmer());
          else if (controller.searchIsEmpty)
            return Center(child: Text('A pesquisa não retornou resultados'));
          else
            return ListView.builder(
              padding: EdgeInsets.symmetric(vertical: 4.0),
              itemExtent: 125,
              itemCount: controller.searchResultProducts.length,
              itemBuilder: (_context, index) {
                return ProductCard(
                  product: controller.searchResultProducts[index],
                );
              },
            );
        },
      ),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [Center(child: Text('SUGESTÕES DE PESQUISA'))],
      ),
    );
  }
}
