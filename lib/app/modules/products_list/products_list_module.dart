import 'filtered_products_page.dart';
import 'products_list_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'products_list_page.dart';

class ProductsListModule extends ChildModule {
  @override
  List<Bind> get binds => [
        $ProductsListController,
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute,
            child: (_, args) => ProductsListPage()),
        ModularRouter('/filtered_products_list',
            child: (_, args) => FilteredProductsPage()),
      ];

  static Inject get to => Inject<ProductsListModule>.of();
}
