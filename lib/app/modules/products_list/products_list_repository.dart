import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:listing_with_slidy/app/shared/models/attribute_model.dart';
import 'package:listing_with_slidy/app/shared/models/attribute_terms_model.dart';
import 'package:listing_with_slidy/app/shared/models/product_model.dart';
import 'package:listing_with_slidy/app/shared/repository/request_contents.dart';
import 'package:listing_with_slidy/app/shared/repository/return_request_data.dart';
import 'package:mobx/mobx.dart';
import 'package:http/http.dart' as http;
import 'package:listing_with_slidy/app/shared/repository/request_contents.dart';

class ProductsListRepository {
  Future<ReturnRequestData<ObservableList<Product>>> getProductsPerPage(
      int perPage, int page, {String orderBy, String order}) async {
    try {
      final _orderBy = orderBy ?? 'date';
      final _order = order ?? 'desc';
      var products = ObservableList<Product>();
      final http.Response res =
          await http.get(baseUrl('products?order=$_order&orderby=$_orderBy&per_page=$perPage&page=$page&'));
      if (res.statusCode == 200) {
        jsonDecode(res.body).forEach((x) => products.add(Product.fromJson(x)));
      } else {
        return ReturnRequestData<ObservableList<Product>>(false);
      }
      return ReturnRequestData<ObservableList<Product>>(true, data: products);
    } catch (error) {
      print(error);
      return ReturnRequestData(false);
    }
  }

  Future<ReturnRequestData<ObservableList<AttributeModel>>>
      getAllAtributes() async {
    try {
      var attributes = ObservableList<AttributeModel>();
      final http.Response res = await http.get(baseUrl('products/attributes?'));
      if (res.statusCode == 200) {
        jsonDecode(res.body)
            .forEach((x) => attributes.add(AttributeModel.fromJson(x)));
      } else {
        return ReturnRequestData<ObservableList<AttributeModel>>(false);
      }
      return ReturnRequestData<ObservableList<AttributeModel>>(true,
          data: attributes);
    } catch (error) {
      print(error);
      return ReturnRequestData(false);
    }
  }

  Future<ObservableList<AttributeTerms>> getAllAtributesTerms(int attributeId) async {
    final http.Response res =
        await http.get(baseUrl('products/attributes/$attributeId/terms?'));
    if (res.statusCode == 200) {
      final List jsonList = jsonDecode(res.body);
      return jsonList.map((json) => AttributeTerms.fromJson(json)).toList().asObservable();
    } else {
      return ObservableList<AttributeTerms>();
    }
  }

  Future<ReturnRequestData<ObservableList<Product>>> getProductsFiltered(
      String slug, int attributeTerm) async {
    try {
      var products = ObservableList<Product>();
      final http.Response res = await http.get(
          baseUrl('products?attribute=$slug&attribute_term=$attributeTerm&'));
      if (res.statusCode == 200) {
        jsonDecode(res.body).forEach((x) => products.add(Product.fromJson(x)));
      } else {
        return ReturnRequestData<ObservableList<Product>>(false);
      }
      return ReturnRequestData<ObservableList<Product>>(true, data: products);
    } catch (error) {
      print(error);
      return ReturnRequestData(false);
    }
  }

  Future<List<Map>> getProductsSearch(String filter) async {
    final http.Response res =
        await http.get(baseUrl('products?search=$filter&'));
    if (res.statusCode == 200) {
      final List jsonList = jsonDecode(res.body);
      return jsonList.map((dyn) {
        final Map map = dyn;
        return map;
      }).toList();
    } else {
      return [];
    }
  }
}
