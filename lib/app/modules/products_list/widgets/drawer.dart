import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:listing_with_slidy/app/modules/products_list/products_list_controller.dart';

import 'attribute_expanded_tile.dart';

class CusDrawer extends StatefulWidget {
  final ProductsListController controller;

  const CusDrawer({Key key, this.controller}) : super(key: key);
  @override
  _CusDrawerState createState() => _CusDrawerState();
}

class _CusDrawerState extends State<CusDrawer> {

  ProductsListController controller = Modular.get();

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          Observer(builder: (_) {
            return controller.attributes.isEmpty
                ? Expanded(
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  )
                : Expanded(
                    child: ListView(
                      scrollDirection: Axis.vertical,
                      children: widget.controller.attributes
                          .map((attribute) => AttributeExpandedTile(
                                attribute: attribute,
                              ))
                          .toList(),
                    ),
                  );
          }),
        ],
      ),
    );
  }
}
