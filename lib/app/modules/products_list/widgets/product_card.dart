import 'package:flutter/material.dart';
import 'package:listing_with_slidy/app/shared/models/product_model.dart';
import 'package:listing_with_slidy/app/utils/likebag-util.dart';
import 'package:listing_with_slidy/app/utils/likebag_darkblue_text.dart';
import 'package:listing_with_slidy/app/utils/likebag_theme_app.dart';

class ProductCard extends StatelessWidget {
  final Product product;

  const ProductCard({Key key, this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
  final elementsWidth = MediaQuery.of(context).size.width * 0.45;
  final marginWidth = MediaQuery.of(context).size.width * 0.025;
    return Card(
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: marginWidth, vertical: 6),
        child: Row(children: [
          Container(
            width: 125,
            height: 125,
            child: Image.network(
              product.images[0].src,
              fit: BoxFit.contain
            )
          ),
          SizedBox(width: 15),
          Container(
            margin: EdgeInsets.only(left: 5),
            width: elementsWidth,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    child: Text(
                      product.name,
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: LikeBagThemeApp.darkBlue,
                          fontWeight: FontWeight.bold,
                          fontSize: 18),
                    ),
                  ),
                  LikeBagDarkBlueText("REF: ${product.sku}", fontSize: 14,),
                  LikeBagDarkBlueText("R\$ " + LikeBagUtil.trataStringPreco(product.price), fontSize: 18, fontWeight: FontWeight.bold,),
                ],
              )),
        ]),
      ),
    );
  }
}
