import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../products_list_controller.dart';
import 'attribute_expanded_tile.dart';

class ProductsListModalBottomSheet extends StatefulWidget {
  const ProductsListModalBottomSheet({Key key}) : super(key: key);

  @override
  _ProductsListModalBottomSheetState createState() =>
      _ProductsListModalBottomSheetState();
}

class _ProductsListModalBottomSheetState
    extends State<ProductsListModalBottomSheet> {
  ProductsListController controller = Modular.get();

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10),
      padding: EdgeInsets.only(top: 15, left: 10, right: 10),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15), topRight: Radius.circular(15))),
      height: 500,
      child: Container(
        child: Observer(builder: (_) {
          if (controller.attributes.isEmpty)
            return Center(
              child: CircularProgressIndicator(),
            );
          else
            return ListView(
              scrollDirection: Axis.vertical,
              children: controller.attributes
                  .map((attribute) => AttributeExpandedTile(
                        attribute: attribute,
                      ))
                  .toList(),
            );
        }),
      ),
    );
  }
}
