import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:listing_with_slidy/app/utils/likebag_theme_app.dart';
import 'package:listing_with_slidy/app/utils/extensions.dart';

import '../products_list_controller.dart';
import 'attribute_expanded_tile.dart';

class OrderByModal extends StatefulWidget {
  const OrderByModal({Key key}) : super(key: key);

  @override
  _OrderByModalState createState() => _OrderByModalState();
}

class _OrderByModalState
    extends State<OrderByModal> {
      ProductsListController controller = Modular.get();
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10),
      padding: EdgeInsets.only(top: 15, left: 10, right: 10),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15), topRight: Radius.circular(15))),
      height: 500,
      child: Container(
          child: Column(
        children: [
          Text('Listar por'),
          Divider(),
          Observer(builder: (_) {
            final selectedValue = controller.selectedOrderBy;
            return Column(
              children: OrderBy.values
                  .map((e) => Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              e.toName(),
                              style: TextStyle(
                                  color: LikeBagThemeApp.darkBlue,
                                  fontWeight: FontWeight.bold),
                            ),
                            Radio(
                              onChanged: (value) =>
                                  controller.setOrderBy(value),
                              groupValue: selectedValue,
                              value: e,
                              activeColor: LikeBagThemeApp.darkBlue,
                            )
                          ]))
                  .toList(),
            );
          }),
          Divider(),
          Text('Ordem'),
          Divider(),
          Observer(builder: (_) {
            return Row(
              children: [
                Row(children: [
                  Text('Crescente'),
                  Radio(
                    onChanged: (value) => controller.setOrder(value),
                    groupValue: controller.order,
                    value: 'asc',
                    activeColor: LikeBagThemeApp.darkBlue,
                  )
                ]),
                Row(children: [
                  Text('Decrescente'),
                  Radio(
                    onChanged: (value) => controller.setOrder(value),
                    groupValue: controller.order,
                    value: 'desc',
                    activeColor: LikeBagThemeApp.darkBlue,
                  )
                ]),
              ],
            );
          }),
          Divider(),
          Container(
            decoration: BoxDecoration(
                color: LikeBagThemeApp.darkBlue,
                borderRadius: BorderRadius.all(Radius.circular(15))),
            child: FlatButton(
                child: Text('ORDENAR', style: TextStyle(color: Colors.white)),
                onPressed: () {
                  controller.refresh(ordering: true);
                  Navigator.pop(context);
                }),
          )
        ],
      )),
    );
  }
}
