import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:listing_with_slidy/app/modules/products_list/products_list_controller.dart';
import 'package:listing_with_slidy/app/shared/models/attribute_model.dart';
import 'package:listing_with_slidy/app/shared/models/attribute_terms_model.dart';

import '../filtered_products_page.dart';
import 'cus_grid_tile.dart';

class AttributeExpandedTile extends StatefulWidget {
  final AttributeModel attribute;

  const AttributeExpandedTile({
    Key key,
    this.attribute,
  }) : super(key: key);

  @override
  _AttributeExpandedTileState createState() => _AttributeExpandedTileState();
}

class _AttributeExpandedTileState extends State<AttributeExpandedTile> {
  ProductsListController controller = Modular.get();
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
      child: Card(
        child: ExpansionTile(
          title: Text(widget.attribute.name),
          children: <Widget>[
            Observer(builder: (_) {
              if (widget.attribute.attributeTerms == null)
                return Container();
              else
                return Container(
                  margin: EdgeInsets.symmetric(horizontal: 5),
                  alignment: Alignment.center,
                  child: GridView(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 3,
                          crossAxisSpacing: 10.0,
                          mainAxisSpacing: 10.0,
                          childAspectRatio: 1.7),
                      children: widget.attribute.attributeTerms
                          .map((attributeTerm) => CusGridTile(
                                onTap: () {
                                  Navigator.pop(context);
                                  Modular.to.popAndPushNamed(
                                      FilteredProductsPage.routeName);
                                  controller.loadProductsFiltered(
                                      widget.attribute, attributeTerm);
                                },
                                attributeTerm: attributeTerm,
                              ))
                          .toList()),
                );
            })
          ],
        ),
      ),
    );
  }
}
