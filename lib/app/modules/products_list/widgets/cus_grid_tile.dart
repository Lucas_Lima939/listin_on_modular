import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:listing_with_slidy/app/shared/models/attribute_terms_model.dart';
import 'package:listing_with_slidy/app/utils/likebag_theme_app.dart';

import '../products_list_controller.dart';

class CusGridTile extends StatefulWidget {
  final AttributeTerms attributeTerm;
  final Function onTap;

  const CusGridTile(
      {Key key, @required this.onTap, @required this.attributeTerm})
      : super(key: key);

  @override
  _CusGridTileState createState() => _CusGridTileState();
}

class _CusGridTileState extends State<CusGridTile> {
  ProductsListController controller = Modular.get();
  @override
  Widget build(BuildContext context) {
    Color color;
    if (controller.selectedFilter != null) {
      color = widget.attributeTerm.id == controller.selectedFilter.id
          ? LikeBagThemeApp.darkBlue
          : null;
    }
    return GestureDetector(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: LikeBagThemeApp.blue,
            ),
            borderRadius: BorderRadius.circular(10.0),
          ),
          color: color,
          child: GridTile(
            child: Center(
              child: Padding(
                padding: EdgeInsets.all(5),
                child: Text(
                  widget.attributeTerm.name,
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
        ),
      ),
      onTap: widget.onTap,
    );
  }
}
