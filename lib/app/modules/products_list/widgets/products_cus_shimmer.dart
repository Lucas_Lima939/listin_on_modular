import 'package:flutter/material.dart';
import 'package:listing_with_slidy/app/utils/likebag_theme_app.dart';
import 'package:shimmer/shimmer.dart';

class ProductsCusShimmer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final elementsWidth = MediaQuery.of(context).size.width * 0.45;
    final marginWidth = MediaQuery.of(context).size.width * 0.025;

    return Card(
      child: Shimmer.fromColors(
        highlightColor: Colors.white,
        baseColor: Colors.grey[300],
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: marginWidth, vertical: 6),
          child: Row(children: [
            Container(
              width: 125,
              height: 125,
              color: Colors.grey,
            ),
            SizedBox(width: 15),
            Container(
                margin: EdgeInsets.only(left: 5),
                width: elementsWidth,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      width: 150,
                      height: 20,
                      color: Colors.grey,
                    ),
                    SizedBox(height: 10),
                    Container(
                      width: 150,
                      height: 20,
                      color: Colors.grey,
                    ),
                    SizedBox(height: 10),
                    Container(
                      width: 150,
                      height: 20,
                      color: Colors.grey,
                    ),
                  ],
                )),
          ]),
        ),
      ),
    );
  }
}
